import socket
import backend_config as config
from datetime import datetime
import redis

sock 		= socket.socket(socket.AF_INET,socket.SOCK_STREAM)
hostname 	= socket.gethostname()    
ipaddr 		= socket.gethostbyname(hostname)
port		= config.port
msgLen		= config.MessageLength
server		= (ipaddr, port)
#backend		= config.backend
#backendport	= config.BackendPort

redisServer	= config.cache
r 			= redis.Redis(host=redisServer, port=6379, db=0)

def getKey(key):
	return r.get(key)

def setKey(key, value):
	r.set(key, value)

print "%s - %s - Starting backend app" %(datetime.now(), ipaddr)
print "%s - %s - Binding to %s" % (datetime.now(), ipaddr, ipaddr)
sock.bind(server)

print "%s - %s - Listening on port %s" % (datetime.now(), ipaddr, port)
sock.listen(1)
while True:
	try:
		while True:
			conn, client = sock.accept()
			print "%s - %s - connected" % (datetime.now(), client)
			data = conn.recv(msgLen).split('\n')[-1].strip()
			action = data.split(',')[0]
			print "%s - %s - data: %s" % (datetime.now(), client, data)
			if data:
				if action == "get":
					key = data.split(',')[1]
					val = getKey(key)
					conn.sendall("%s -- %s\n" % (key, val))
				elif action == "set":
					key, value = data.split(',')[1].split(':')
					setKey(key, value)
					conn.sendall("Set %s:%s\n" %(key, value))
				else:
					print "%s - %s - Invalid action requested by client - %s" %(datetime.now(), ipaddr, client)
					conn.sendall("Invalid action requested \n")

				conn.close()
				print "%s - %s - closed" % (datetime.now(), client)
				continue
	except KeyboardInterrupt:
		print "%s - %s - Shutting down server" % (datetime.now(), ipaddr)
		sock.close()
		break
	except Exception as e:
		print "%s - %s - Exception %s" % (datetime.now(), server, (e.message, e.args))