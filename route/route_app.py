import socket
import route_config as config
from datetime import datetime

sock 		= socket.socket(socket.AF_INET,socket.SOCK_STREAM)
hostname 	= socket.gethostname()    
ipaddr 		= socket.gethostbyname(hostname)
port		= config.port
msgLen		= config.MessageLength
server		= (ipaddr, port)

print "%s - %s - Binding to %s" % (datetime.now(), ipaddr, ipaddr)
sock.bind(server)

print "%s - %s - Listening on port %s" % (datetime.now(), ipaddr, port)
sock.listen(1)
while True:
	try:
		while True:
			conn, client = sock.accept()
			print "%s - %s - connected" % (datetime.now(), client)
			data = conn.recv(msgLen)
			print "%s - %s - data: %s " % (datetime.now(), client, data)
			if data:
				conn.sendall("Received data by server (%s)\n" % ipaddr)
				conn.close()
				print "%s - %s - closed" % (datetime.now(), client)
				continue
	except KeyboardInterrupt:
		print "%s - %s - Shutting down server" % (datetime.now(), ipaddr)
		sock.close()
		break
	except Exception as e:
		print "%s - %s - Exception %s" % (datetime.now(), server, (e.message, e.args))