import socket
import actual_config as config
from datetime import datetime
#import redis

sock 		= socket.socket(socket.AF_INET,socket.SOCK_STREAM)
hostname 	= socket.gethostname()    
ipaddr 		= socket.gethostbyname(hostname)
port		= config.port
msgLen		= config.MessageLength
server		= (ipaddr, port)
backend		= config.backend
backendport	= config.BackendPort

#redisServer	= config.cache
#r 			= redis.Redis(host=redisServer, port=6379, db=0)

#def getKey(key):
#	return r.get(key)

#def setKey(key, value):
#	return r.set(key, value)

def sendToBackend(data):
	backendServer = (backend, backendport)
	clientsock = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
	clientsock.connect(backendServer)
	print "%s - %s - Connected to backend" %( datetime.now(), ipaddr)
	clientsock.sendall(data)
	response = clientsock.recv(100)
	print "%s - %s - Backend response : %s" % (datetime.now(), ipaddr, response)
	clientsock.close()
	print "%s - %s - Closed backend connection" % ( datetime.now(), ipaddr)
	return response

print "%s - %s - Starting actual app" %(datetime.now(), ipaddr)
print "%s - %s - Binding to %s" % (datetime.now(), ipaddr, ipaddr)
sock.bind(server)

print "%s - %s - Listening on port %s" % (datetime.now(), ipaddr, port)
sock.listen(1)
while True:
	try:
		while True:
			conn, client = sock.accept()
			print "%s - %s - connected" % (datetime.now(), client)
			data = conn.recv(msgLen).split()[-1]
			#action = data.strip().split(':')[0]
			#content = data.strip().split(':')[1:]
			#print "%s - %s - data: %s : %s " % (datetime.now(), client, action, content)
			print "%s - %s - data: %s " % (datetime.now(), client, data)
			if data:
				conn.sendall("Received data by server (%s)\n" % ipaddr)
				print "%s - %s - closed" % (datetime.now(), client)
				print "%s - %s - sending kv data to backend" % (datetime.now(), ipaddr)
				conn.sendall("Backend response: %s\n" % sendToBackend(data))
				conn.close()
				continue
	except KeyboardInterrupt:
		print "%s - %s - Shutting down server" % (datetime.now(), ipaddr)
		sock.close()
		break
	except Exception as e:
		print "%s - %s - Exception %s" % (datetime.now(), server, (e.message, e.args))